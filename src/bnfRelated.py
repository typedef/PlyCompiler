# It's scanner.tokens translated to bnf like tokens
# Example: Identifier -> <Identifier>
import copy
import re
from re import Pattern

from src import codeHelpers, scanner
from src.parser import BnfItemType, ParserNode
from src.ply.lex import LexToken

precedence = (
    # priority higher better
    ('nonassoc', 'LessEqual', 'MoreEqual', 'Less', 'More', 'Equal', "NotEqual"),  # Nonassociative operators

    # 0 priority
    ('left', 'Plus', 'Minus'),

    # 1 priority
    ('left', 'Times', 'Divide', 'Mod'),

    #('', 'Assign')
)


class BnfFormula:
    def __init__(self, formula, parent):
        self.FormulaAsStr = formula
        self.Parent = parent
        self.InnerItems = []

    def add(self, bnfItem):
        self.InnerItems.append(bnfItem)

    def isSuitable(self, tokens: [LexToken], stackInd, tempInd):
        parserNode: ParserNode = ParserNode()
        parserNode.Current = self.FormulaAsStr

        if self.FormulaAsStr == '<field modifier list>? <type> <Identifier> <DotComma>':
            i = 2123

        # All items should be 'Suitable'
        for innerItem in self.InnerItems:
            if innerItem.Type == BnfItemType.Token or innerItem.Type == BnfItemType.Keyword:
                ''' 
                    ТП - таблица переходов

                    Здесь мы должны использовать таблицу переходов для обновления состояния 
                    Как будет работать?
                    Сейчас:
                    1) Валидируем правило if valid -> return True, Node else -> return False, None

                    Новая версия:
                    1) Подбираем правило согласно таблице переходов

                    Отличия:
                    1) index может быть один, тк ТП приведет нас к END или None (Error), то есть мы всегда получаем результат

                '''
                token = tokens[tempInd[0]]

                isTokenSuitable: bool = innerItem.Var.isTypeEqual(token)
                if not isTokenSuitable:
                    # print("Token {0} is not suitable for token {1}".format(innerItem.Var, tokens[index[0]]))
                    return False, None

                tempInd[0] = tempInd[0] + 1

                childNode: ParserNode = ParserNode()
                childNode.Current = innerItem
                childNode.Parent = self.Parent
                parserNode.Children.append(childNode)

            else:
                result, childNode = innerItem.isSuitable(tokens, stackInd, tempInd)
                if not result:
                    # print("Inner item {0} is not suitable for token {1}".format(innerItem.Var, tokens[index[0]]))
                    tempInd = stackInd
                    return False, None
                if childNode is None:
                    continue

                childNode.Parent = self.Parent
                parserNode.Children.append(childNode)

        stackInd = tempInd

        return True, parserNode

    def isRecursive(self, baseName):
        for innerItem in self.InnerItems:
            if innerItem.Var == baseName:
                return True
        return False

    def innerAsString(self):
        str = ''
        for innerItem in self.InnerItems:
            str += innerItem.Var
        return str

    def print(self, tab):
        for innerItem in self.InnerItems:
            innerItem.print(tab)

    def toString(self, tab):
        sb = ""
        i = 0
        cnt = len(self.InnerItems) - 1
        for innerItem in self.InnerItems:
            sb += innerItem.toString(tab)
        return sb

    def __str__(self):
        return self.FormulaAsStr

    def __repr__(self):
        return self.__str__()


class BnfItem:
    rIsOptionalRule: Pattern = re.compile(r'<[a-zA-Z\d\s]*>\?')
    rIsRule: Pattern = re.compile(r'<[a-zA-Z\d\s]*>')
    rIsKeyword: Pattern = re.compile(r'[a-zA-Z\d]+')
    rIsRuleOrKeyword: Pattern = re.compile(r'<[a-zA-Z\d\s]*>\?|<[a-zA-Z\d\s]*>|[a-zA-Z\d]+')

    optionalItems = {}

    def __init__(self, rule, type=BnfItemType.Default):
        self.Formulas: [BnfFormula] = []
        self.Type: BnfItemType = type
        if type == BnfItemType.Optional or type == BnfItemType.Default:
            splittedRule = rule.split('::=')
            self.Var = splittedRule[0].strip()
            self.Formula = splittedRule[1].strip()
            self.Rule = rule
        else:
            self.Var = rule

    def isComplex(self):
        return len(self.Formulas) > 0

    def compileSingleFormula(self, formulaAsStr: str, bnfRulesDictionary):
        formulaStr = formulaAsStr.strip()
        bnfFormula: BnfFormula = BnfFormula(formulaStr, self)

        matches = BnfItem.rIsRuleOrKeyword.findall(formulaStr)
        for match in matches:
            bnfItem: BnfItem = self.__getRuleFromTree(match, bnfRulesDictionary)
            bnfFormula.add(bnfItem)

        return bnfFormula

    def compile(self, bnfRulesDictionary):
        rule = self.Formula
        # Proccessing rule
        fomulasAsStrArr = rule.split('|')
        for formulasAsStr in fomulasAsStrArr:
            bnfFormula: BnfFormula = self.compileSingleFormula(formulasAsStr, bnfRulesDictionary)
            self.Formulas.append(bnfFormula)

    def isOptional(self):
        return self.Type == BnfItemType.Optional

    def isSuitable(self, tokens: [LexToken], stackInd, tempInd):
        # Doing all the work for optional: if (exist) skip_tokens
        parentNode: ParserNode = ParserNode()
        parentNode.Current = self

        isAnyFormulaSuitable: bool = False
        for formula in self.Formulas:
            if self.__is_formula_creates_recursion(formula):
                continue

            result, childNode = formula.isSuitable(tokens, stackInd, tempInd)
            if result and childNode is not None:
                isAnyFormulaSuitable = True
                childNode.Parent = self
                parentNode.Current = self
                parentNode.Children.append(childNode)

        if isAnyFormulaSuitable:
            return True, parentNode

        if self.isOptional():
            return True, None

        return False, None

    def toString(self, tab):
        containsStr = ":"
        if (len(self.Formulas) <= 0):
            containsStr = " no elements"

        str = "{0}{1}{2}\n".format(tab, self.Var, containsStr)
        tab += "\t"

        for formula in self.Formulas:
            if self.__is_formula_creates_recursion(formula):
                continue
            str += formula.toString(tab)
        return str

    def print(self, prevTab=""):
        containsStr = ":"
        if (len(self.Formulas) <= 0):
            containsStr = " no elements"

        print("{0}Var: {1} contains{2}".format(prevTab, self.Var, containsStr))

        prevTab += "\t"
        for formula in self.Formulas:
            for innerItem in formula.InnerItems:
                print("{0}{1}".format(prevTab, innerItem.Var))

        for formula in self.Formulas:
            if formula.isRecursive(self.Var):
                print("[Recursive] {0}".format(formula.InnerItems[0].Var))
            else:
                formula.print(prevTab)

    def __is_formula_creates_recursion(self, formula):
        if self.Var == formula.InnerItems[0].Var:
            return True
        return False

    def __make_or_get_optional(self, bnfItem):
        if bnfItem.Var in BnfItem.optionalItems:
            return BnfItem.optionalItems[bnfItem.Var]

        bnfItemOptional = copy.deepcopy(bnfItem)
        bnfItemOptional.Type = BnfItemType.Optional
        BnfItem.optionalItems[bnfItem.Var] = bnfItemOptional
        return bnfItemOptional

    def __getRuleFromTree(self, rule, bnfRulesDictionary):
        lastChar = rule[-1]
        if lastChar == '?':
            nonOptionalRule = rule[0: -1]
            if not nonOptionalRule in bnfRulesDictionary:
                raise Exception("Rule is not registered {0}".format(rule))

            nonOptionalItem = bnfRulesDictionary[nonOptionalRule]
            return self.__make_or_get_optional(nonOptionalItem)

        if rule in bnfRulesDictionary:
            return bnfRulesDictionary[rule]

        if rule in scanner.keyWords:
            return BnfItem(rule=scanner.getToken(rule, "Keyword"), type=BnfItemType.Keyword)

        simplified = rule.replace("<", "").replace(">", "")
        if simplified in scanner.tokens:
            return BnfItem(rule=scanner.getToken(simplified, "Token"), type=BnfItemType.Token)

        codeHelpers.error("Bnf rule is not registered! {0}".format(rule))
        raise Exception("Bnf rule is not registered!")

    def __str__(self):
        return "BnfItem: {0}".format(self.Var)

    def __repr__(self):
        return self.__str__()


def syntax_create_from_bnf_file():
    bnfRulesDictionary = {}

    fileStr = codeHelpers.readFileAsString("../ParserSyntaxIn.bnf")
    splitted = fileStr.split("\n")
    splittedLen = len(splitted) - 1
    for i in range(splittedLen, 0, -1):
        split = splitted[i]
        rule = split.strip()
        if len(rule) <= 0 or rule[0] == ';':
            continue

        key = rule.split("::=")[0].strip()
        formattedKey = "p_" + key.replace("<", "").replace(">", "").replace("?", "").replace(" ", "_")
        # print(formattedKey)
        bnfRulesDictionary[key] = BnfItem(rule)

    for bnfRule in bnfRulesDictionary:
        bnfRulesDictionary[bnfRule].compile(bnfRulesDictionary)

    return bnfRulesDictionary


def weird_arrays(bnfFormula: BnfFormula):
    '''
    Для формулы <a>? <b> <c> <d?> <e> существуют след ключи:
    [0] <a>? <b> <c> <d?> <e>
    [1]  _   <b> <c> <d?> <e>
    [2] <a>? <b> <c>  _   <e>
    [3]  _   <b> <c>  _   <e>
    '''
    finalFormulas = []
    finalFormulas.append([])

    for innerItem in bnfFormula.InnerItems:
        innerStr = str(innerItem.Var)

        if innerItem.isOptional():
            # Создаем дубликаты сущ массивов
            ffrange = range(len(finalFormulas))
            for ffi in ffrange:
                finalFormulas.append(copy.deepcopy(finalFormulas[ffi]))

            for i, finalFormula in enumerate(finalFormulas):
                if (i % 2) == 0:
                    finalFormula.append(innerStr)
        else:
            # В каждый дубликат записываем элемент
            for finalFormula in finalFormulas:
                finalFormula.append(innerStr)

    # I AM NOT FUCKING SURE ABOUT THIS
    res = []
    for finalFormula in finalFormulas:
        res.append(tuple(finalFormula))

    return res


def bnfItemsToString(bnfItems: [BnfItem]):
    strArr = list(map(lambda _: str(_.Var), bnfItems))
    return ', '.join(strArr)


def buildtransitionTable(bnfRulesDictionary):
    stt = {}
    mtt = {}

    for rule in bnfRulesDictionary:
        bnfRule: BnfItem = bnfRulesDictionary[rule]
        for formula in bnfRule.Formulas:
            finalFormulas = weird_arrays(formula)

            if len(finalFormulas) == 1:
                for finalFormula in finalFormulas:
                    if finalFormula not in stt:
                        stt[finalFormula] = []
                    stt[finalFormula].append(rule)
            else:
                for finalFormula in finalFormulas:
                    if finalFormula not in mtt:
                        mtt[finalFormula] = []
                    mtt[finalFormula].append(rule)

    return stt, mtt


def removeKeyArr(tt):
    ttRes = {}
    for ttItem in tt:
        if len(ttItem) != 1:
            continue
        k = ttItem[0]
        v = tt[ttItem]
        ttRes[k] = v

    return ttRes


def convertKeyArrToStr(tt):
    ttWithStrKey = {}
    for ttItem in tt:
        k = codeHelpers.strsToString(ttItem)
        v = tt[ttItem]
        ttWithStrKey[k] = v

    return ttWithStrKey
class KeyValue:
    def __init__(self, key, value):
        self.Key = key
        self.Value = value

# why i need this????
def separateKeysInTransitionTable(transitionTable):
    keyValuesToAdd = []

    for key in transitionTable:
        keyLen = len(key)
        if keyLen <= 1:
            continue

        temp = []
        shortLen = keyLen - 1
        for i in range(shortLen):
            temp.append(key[i])
            nextKey = copy.deepcopy(temp)
            nextKey.append(key[i + 1])
            keyValuesToAdd.append(KeyValue(tuple(temp), nextKey))

    for kv in keyValuesToAdd:
        transitionTable[kv.Key] = kv.Value

    ttWithStrKey = {}
    for ttItem in transitionTable:
        k = codeHelpers.strsToString(ttItem)
        v = transitionTable[ttItem]
        if len(v) == 1:
            ttWithStrKey[k] = v
        else:
            ttWithStrKey[k] = v

    return ttWithStrKey