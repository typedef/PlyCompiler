import ply.lex as lex
from ply.lex import TOKEN
from ply.lex import Lexer, LexToken
import codeHelpers

tokens = [
    # 3.14 5.1E+10 4.17e-3 3 0xaa
    # Number
    'FloatNumber',
    'DoubleNumber',
    'IntegerNumber',
    'BoolNumber',

    # // /**/
    "Comment",

    "Keyword",

    # +        -        *         /       %
    'Plus', 'Minus', 'Times', 'Divide', 'Mod',

    # <=            >=
    'LessEqual', 'MoreEqual',
    
    # <         >       =        ==        !=
    'Less',  'More', 'Assign', 'Equal', "NotEqual",

    # Binary operators
    # |             &              ^           ~
    'OrBinary',  'AndBinary',  'XorBinary', 'BinaryReverse',

    # Teranry operator
    # max = (a > b) ? a : b
    "TernaryOperaror",
    
    # &&    ||     !        ;          ,       .
    "And", "Or", "Not", "DotComma", "Comma", "AccessOperator",


    # ( )
    "ParenthesesLeft", "ParenthesesRight",
    # { }
    "CurlyBracketsLeft", "CurlyBracketsRight",

    # Char = 'f', String = "hallo world"
    "StringLiteral", "CharLiteral",

    # @Override
    'Anotation',

    # camelCase, function_name, _privateField, someWeirdo235_194
    'Identifier',

    # int[]              new int[5], new int[getArrayLength()]
    'ArrayDefinition', 'ArrayDeclaration',

    # Ignore
    "Ignore",

    # Errors
    'Error_WrongIdentifier',
    'Error_WrongCharacter',
    'Error_UnterminatedStringConstant',
]

# Tokens

# Some kind of hash table
keyWords = {

    # Classical Java Types
    "void", "bool", "char", "byte", "short", "int", "long", "float", "double", "String",
    
    # New cool types
    "string", "i8", "i16", "i32", "i64", "u8", "u16", "u32", "u64", "f32", "f64",

    # Data structure related
    "class", "interface", "extends", "implements", "base",
    "public", "protected", "private", "static",
    
    # Memory related
    "null", "new", "this",

    # Conditions
    "if", "else",

    # Loops
    "while", "for", "do", "switch", "case", "break",

    # return from function
    "return"

}
shortVarNames = {}
globalNewLinePosFromStart = 0
globalInitialized = 0

class ParserToken:
    def __init__(self, value, type):
        self.Value = value
        self.Type = type
    def __str__(self):
        return "{0}".format(self.Value, self.Type)

    def isTypeEqual(self, lexToken: LexToken):
        result: bool = lexToken.type == self.Value or lexToken.type == self.Type
        #if lexToken.value != self.Value:
        #    i = 0
        return result

def getToken(val, type):
    return ParserToken(val, type)

def setColumnRangeForToken(t: LexToken):
    global globalNewLinePosFromStart
    t.column = t.lexpos - globalNewLinePosFromStart + 1

def t_comment(t: LexToken):
    r'(\/\/.*)|(\/\*(.|\n)*?\*\/)'
    #'/(/\*(.|\n)*?\*/)|(//.*)'
    linesCount = len(t.value.split('\n')) - 1
    #print("Lines count {0}".format(linesCount))
    t.lexer.lineno += linesCount
    pass

def t_BoolNumber(t: LexToken):
    r'true|false'

    setColumnRangeForToken(t)
    t.strValue = t.value

    if t.value == 'true':
        t.value = True
    else:
        t.value = False
    return t

def t_Identifier(t: LexToken):
    r'[a-zA-Z_]+[a-zA-Z0-9_]*'
    # Идентификаторы могут содержать не более 31 символа.

    setColumnRangeForToken(t)

    if (t.value in keyWords):
        t.type = 'Keyword' #t.value
    elif (len(t.value) > 31):
        #print("Identifier too long, more than 31 char!")
        if (t.value in shortVarNames):
            t.value = shortVarNames[t.value]
        else:
            key = t.value
            value = "g_" + codeHelpers.getRandomAsciiString(29)
            shortVarNames[key] = value
            t.value = value
            t.oldValue = key
        t.type = "Error_WrongIdentifier"
    return t

def t_FloatNumber(t: LexToken): #add support .12 0xffff, 0X1.
    r'((\d+[.]\d*)|(\d*[.]\d+))([eE][+-]\d+)?[f]'

    setColumnRangeForToken(t)
    t.strValue = t.value

    try:
        t.value = t.value.replace("f", "")
        t.value = float(t.value)
    except ValueError:
        #print("Float value too large %d", t.value)
        t.value = 0
    return t

def t_DoubleNumber(t: LexToken):
    r'((\d+[.]\d*)|(\d*[.]\d+))([eE][+-]\d+)?'

    setColumnRangeForToken(t)
    t.strValue = t.value

    try:
        t.value = float(t.value)
    except ValueError:
        #print("Double value too large %d", t.value)
        t.value = 0
    return t

def t_IntegerNumber(t: LexToken):
    r'(0[xX][0-9a-fA-F]{1,16})|(\d+)'

    setColumnRangeForToken(t)
    t.strValue = t.value

    try:
        if (len(t.value) >= 2 and t.value[0] == "0" and (t.value[1] == "X" or t.value[1] == "x")):
            t.value = int(t.value, 16)
        else:
            t.value = int(t.value)
    except ValueError:
        #print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_LessEqual(t: LexToken):
    r'\<\='
    setColumnRangeForToken(t)
    return t


def t_MoreEqual(t: LexToken):
    r'\>\='
    setColumnRangeForToken(t)
    return t
def t_More(t: LexToken):
    r'\>'
    setColumnRangeForToken(t)
    return t


def t_Equal(t: LexToken):
    r'=='
    setColumnRangeForToken(t)
    return t

def t_NotEqual(t: LexToken):
    r'!='
    setColumnRangeForToken(t)
    return t

def t_Assign(t: LexToken):
    r'\='
    setColumnRangeForToken(t)
    return t

def t_And(t: LexToken):
    r'&&'
    setColumnRangeForToken(t)
    return t
def t_Or(t: LexToken):
    r'\|\|'
    setColumnRangeForToken(t)
    return t
def t_AndBinary(t: LexToken):
    r'\&'
    setColumnRangeForToken(t)
    return t
def t_OrBinary(t: LexToken):
    r'\|'
    setColumnRangeForToken(t)
    return t

def t_XorBinary(t: LexToken):
    r'\^'
    setColumnRangeForToken(t)
    return t

def t_BinaryReverse(t: LexToken):
    r'\~'
    setColumnRangeForToken(t)
    return t

def t_TernaryOperaror(t: LexToken):
    r'\?'
    setColumnRangeForToken(t)
    return t

def t_Not(t: LexToken):
    r'\!'
    setColumnRangeForToken(t)
    return t


def t_DotComma(t: LexToken):
    r'\;'
    setColumnRangeForToken(t)
    return t

def t_Comma(t: LexToken):
    r'\,'
    setColumnRangeForToken(t)
    return t

def t_Plus(t: LexToken):
    r'\+'
    setColumnRangeForToken(t)
    return t

def t_Minus(t: LexToken):
    r'\-'
    setColumnRangeForToken(t)
    return t

def t_Times(t: LexToken):
    r'\*'
    setColumnRangeForToken(t)
    return t

def t_Divide(t: LexToken):
    r'/'
    setColumnRangeForToken(t)
    return t

def t_Mod(t: LexToken):
    r'\%'
    setColumnRangeForToken(t)
    return t

def t_Less(t: LexToken):
    r'\<'
    setColumnRangeForToken(t)
    return t

def t_AccessOperator(t: LexToken):
    r'[.]'
    setColumnRangeForToken(t)
    return t

# [ without any chars ] == array definition
# int[] arr;
def t_ArrayDefinition(t: LexToken):
    r'\[[\s\t\n]*\]'
    setColumnRangeForToken(t)
    return t

# [ with chars ] = array declaration
# new int[5]; new int[getArrayLength()];
# Не работает и наверное не должно: new int[] { 0, 1, 2, 3 }
def t_ArrayDeclaration(t: LexToken):
    r'\[\w*\]'
    setColumnRangeForToken(t)
    return t

def t_ParenthesesLeft(t: LexToken):
    r'\('
    setColumnRangeForToken(t)
    return t


def t_ParenthesesRight(t: LexToken):
    r'\)'
    setColumnRangeForToken(t)
    return t


def t_CurlyBracketsLeft(t: LexToken):
    r'{'
    setColumnRangeForToken(t)
    return t


def t_CurlyBracketsRight(t: LexToken):
    r'}'
    setColumnRangeForToken(t)
    return t

def t_StringLiteral(t: LexToken):
    r'\"([^\\\n]|(\\.))*?\"'
    setColumnRangeForToken(t)
    t.strValue = t.value
    return t

def t_CharLiteral(t: LexToken):
    r'\'([^\\\n]|(\\.))*?\''
    setColumnRangeForToken(t)
    t.strValue = t.value
    return t

def t_Anotation(t: LexToken):
    r'@[A-Z]+[a-z]*'
    setColumnRangeForToken(t)
    return t

t_ignore: str = ' \t' # Should be a string Ignored characters

def t_newline(t: LexToken):
    r'\n+'
    t.lexer.lineno += len(t.value) # t.value.count("\n") # track line numbers
    global globalNewLinePosFromStart
    globalNewLinePosFromStart = t.lexer.lexpos


def t_error(t):
    #print("[scanner] Error_WrongCharacter '%s'" % t.value[0])

    if t.value[0] == '"':
        ind = t.value.find('"', 1, len(t.value)) + 1
        otherInd = t.value.find('\n', 1, len(t.value))
        ind = min(ind, otherInd)
        strConstant = t.value[0: ind]
        t.value = strConstant
        t.lexer.skip(ind)
        t.type = "Error_UnterminatedStringConstant"
    else:
        t.lexer.skip(1)
        t.type = 'Error_WrongCharacter'

    return t


globalLexer: Lexer = lex.lex()

def init():
    global globalInitialized
    global globalNewLinePosFromStart
    globalInitialized = 1
    globalNewLinePosFromStart = 0

def uninit():
    global globalInitialized
    globalInitialized = 0


def initCheck():
    global globalInitialized
    if (globalInitialized != 1):
        print("You should call scanner.init() before running any lexer or parser code!")
        raise Exception(
            "[Code is not initialized] You should call scanner.init() before running any lexer or parser code!")

def token():
    initCheck()
    tok: LexToken = globalLexer.token()
    return tok

def input(data):
    initCheck()
    globalLexer.input(data)

def getTokens(data):
    init()

    lexer = lex.lex()
    lexer.input(data)

    analizedTokens: LexToken = []

    tok = lexer.token()

    while tok is not None:
        analizedTokens.append(tok)
        tok = lexer.token()

    uninit()

    return analizedTokens

