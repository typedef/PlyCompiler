import random

import mparser
import imgui
import imgui.integrations.glfw
import glfw
# On normal os
# python -m pip install pyopengl
# On win
# py -m pip install pyopengl
import OpenGL.GL as gl

def CreateWindow(w, h, title):
    if not glfw.init():
        print("Can;t init glfw!")
        exit(1)

    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)
    glfw.window_hint(glfw.RESIZABLE, gl.GL_FALSE)

    window = glfw.create_window(w, h, title, None, None)
    if not window:
        glfw.terminate()
        print("Can;t create glfw window!")
        exit(1)

    glfw.make_context_current(window)

    return window


def MainLoop(window, w, h):
    imgui.create_context()
    imgui.get_io().display_size = w, h

    backend = imgui.integrations.glfw.GlfwRenderer(window)
    io = imgui.get_io()
    io.font_global_scale = 1.5
    io.fonts.clear()
    io = imgui.get_io()
    new_font = io.fonts.add_font_from_file_ttf("../resources/NotoSans.ttf", 20)
    backend.refresh_font_texture()
    #backend.refresh_font_texture()
    #imgui.get_io().fonts.get_tex_data_as_rgba32()

    while not glfw.window_should_close(window):
        glfw.poll_events()
        backend.process_inputs()
        imgui.new_frame()

        UiRender(w, h)

        gl.glClearColor(0.4,0.4,0.4,1.0)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        imgui.render()
        imgui.end_frame()
        backend.render(imgui.get_draw_data())
        glfw.swap_buffers(window)

    backend.shutdown()
    glfw.terminate()

gNodes: [mparser.ParserNode] = []
class Rgba:
    def __init__(self, r, g, b, a = 1.0):
        self.R = r
        self.G = g
        self.B = b
        self.A = a

gColors = [
    Rgba(0.7, 0.89, 0.5), Rgba(1, 0.7, 0.5), Rgba(0.5565, 0.7567, 0.8789), Rgba(0.67, 0.85, 0.595), Rgba(0.75, 0.69, 0.55), Rgba(0.55, 0.65, 0.87)
]
gInd = -1
gItemInd = 0

def RepresentNode(node: mparser.ParserNode, rind: int = 0):
    global gColors
    global gInd
    global gItemInd

    colorRgba: Rgba = gColors[rind % len(gColors)]
    gInd += 1
    nodeName = str(node.Current.Type) + " [{0}]".format(hex(id(node)))
    if imgui.tree_node(nodeName):
        for n in node.Children:
            if len(n.Children) > 0:
                RepresentNode(n, rind + 1)
            else:
                imgui.push_style_color(imgui.COLOR_TEXT, colorRgba.R, colorRgba.G, colorRgba.B, colorRgba.A)
                imgui.text(str(n.Current.Type) + "  [{0}]".format(str(n.Current.Value.value)))
                gItemInd += 1
                imgui.pop_style_color()

        imgui.tree_pop()
    gInd = 0

def UiRender(w, h):
    global gNodes

    imgui.set_next_window_size(w, h)
    imgui.set_next_window_position(0, 0)
    imgui.begin("Parser Window!", False, flags=imgui.WINDOW_NO_MOVE|imgui.WINDOW_NO_RESIZE) #flags=imgui.WINDOW_MENU_BAR
    imgui.text("Parser Result")

    if imgui.button("Run parser"):
        gNodes = mparser.run()

    # App Started
    for node in gNodes:
        RepresentNode(node)

    imgui.end()



def SyntaxAnalizerRunTest():
    w = 1500
    h = 500
    window = CreateWindow(w, h, "Parser Test Window")
    MainLoop(window, w, h)
