import codeHelpers
import testApi

def КорректныеТесты():
    print()
    print("[Корректные Тесты]")
    print(testApi.runTestFile('../fixed_tests/badbool.frag'))
    print(testApi.runTestFile('../fixed_tests/badident.frag'))
    print(testApi.runTestFile('../fixed_tests/badint.frag'))
    print(testApi.runTestFile('../fixed_tests/badpre.frag'))
    print(testApi.runTestFile('../fixed_tests/badstring.frag'))
    print(testApi.runTestFile('../fixed_tests/ident.frag'))
    print(testApi.runTestFile('../fixed_tests/string.frag'))
    print(testApi.runTestFile('../fixed_tests/comment.frag'))
    print(testApi.runTestFile('../fixed_tests/program2.frag'))
    print(testApi.runTestFile('../fixed_tests/define.frag'))
    print(testApi.runTestFile('../fixed_tests/reserve_op.frag'))
    print(testApi.runTestFile('../fixed_tests/number.frag'))


def НекорректныеТесты():
    print()
    print("[Некорректные Тесты]")
    # Онлайн компилятор JAVA: https://www.onlinegdb.com/online_java_compiler

    # double значение .12 в Java спокойно разбирается
    '''
    public class Main
    {
        public static void main (String[]args)
        {
            double d0 = .12;
            double d1 = .12E+2;
            System.out.println(d0);
            System.out.println(d1);
        }
    }
    '''
    print(testApi.runTestFile('../fixed_tests/baddouble.frag'))

    # char, float, short - это ключевые слова в языке Java
    '''
    public class Main
    {
        public static void main (String[]args)
        {
            char c = 'A';
            float pi = 3.14f;
            short values = 2340;
            long l = 12303030303L;
            System.out.println(l);
        }
	}
    '''
    print(testApi.runTestFile('../fixed_tests/badreserve.frag'))

    # ^ & ? ~  это корректные операторы в языке программирования Java
    '''
        public class Main
        {
            public static void main (String[]args)
            {
                int i = ((100 | 110) & (113 | 114) ^ 115);
                float pi = (true) ? 3.14f : 1.23f;
                System.out.println(i);
                System.out.println(pi);
            }
    	}
    '''
    print(testApi.runTestFile('../fixed_tests/badop.frag'))

    # Отличается выводом числа 2, которое имеет тип Double, именно поэтому оно выводится с точкой
    print(testApi.runTestFile('../fixed_tests/program3.frag'))


def ТестыЛексическогоАнализатора():
    codeHelpers.recreateAllTestButWithEqualIdentation()
    КорректныеТесты()
    НекорректныеТесты()