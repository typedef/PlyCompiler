import math
import os
import codeHelpers, scanner
from codeHelpers import PrintColor


class TestResult:
    def __init__(self, result, fileName, output, diffOfFiles):
        self.Result = result
        self.Output = output
        self.FileName = fileName
        self.DiffOfFiles = diffOfFiles

    def __str__(self):
        if self.Result:
            resultAsString = codeHelpers.PrintColor.GREEN + "Successed" + codeHelpers.PrintColor.END
        else:
            resultAsString = codeHelpers.PrintColor.RED + "Failed" + codeHelpers.PrintColor.END
        return "{0}: {1}".format(self.FileName, resultAsString)

    def toExtendedString(self):
        newStr = ''
        splitted = self.Output.split("\n")
        i = 1
        for split in splitted:
            spaces = ''
            if int(math.log10(i)) >= 1:
                spaces = " "
            else:
                spaces = "  "

            newStr += "{0}.{1}{2}\n".format(i, spaces, split)
            i += 1
        return "{0}\n{1}\n{2}\n".format(self.__str__(), newStr, self.DiffOfFiles)

def realJavaCodeTest():
    newDataFile = codeHelpers.readFileAsString("../resources/JavaSampleProgram.java")
    tokens = scanner.doLexicalAnalization(newDataFile)
    codeHelpers.debug_WriteTokensToFile(tokens)

def compareTwoString(s0, s1, s0Alias, s1Alias):
    result: str = ''

    s0Splitted = s0.split("\n")
    s1Splitted = s1.split("\n")

    s0Len = len(s0Splitted)
    s1Len = len(s1Splitted)
    if (s0Len != s1Len):
        result += "Две строки отличаются кол-вом переносов!\n"

    for i in range(s0Len):
        if (i >= s0Len):
            result += "Line {0} {1}=Empty ".format(i + 1, s0Alias)
        if (i >= s1Len):
            result += "{0}=Empty\n".format(s1Alias)
        if (i < s0Len and i < s1Len):
            s0c = s0Splitted[i]
            s1c = s1Splitted[i]
            if (s0c != s1c):
                result += "{0}Line {1}{2} ".format(PrintColor.YELLOW, i + 1, PrintColor.END)
                result += "{0}=\'{1}\' {2}=\'{3}\'\n".format(s0Alias, s0c, s1Alias, s1c)

    return result

def runTestFile(file):
    outFileName = os.path.dirname(file) + "/" + os.path.basename(file).split('.')[0] + ".out"
    outDataContent = codeHelpers.readFileAsString(outFileName)

    dataContent = codeHelpers.readFileAsString(file)
    tokens = scanner.getTokens(dataContent)

    valuesPrintable = {
        'StringLiteral',
    }

    strValuesPrintable = {
        'IntegerNumber',
        'DoubleNumber',
    }

    outFile = ''
    for token in tokens:
        valueAsStr = "{0}".format(token.value)
        if (token.type == 'Error_WrongIdentifier'):
            outFile += "*** Error line {0}.\n".format(token.lineno)
            outFile += "*** Identifier too long: \"{0}\"\n".format(token.oldValue)
            outFile += "{0} line {1} cols {2}-{3} is Identifier (truncated to {5})".format(token.oldValue,
                                                                                    token.lineno,
                                                                                    token.column,
                                                                                    (token.column + len(token.oldValue) - 1),
                                                                                    token.type,
                                                                                    token.oldValue[0:31])
        elif (token.type == 'Error_WrongCharacter'):
            outFile += "*** Error line {0}.\n".format(token.lineno)
            outFile += "*** Unrecognized char: \'{0}\'".format(token.value[0])

        elif (token.type == 'Error_UnterminatedStringConstant'):
            outFile += "*** Error line {0}.\n".format(token.lineno)
            outFile += "*** Unterminated string constant: {0}".format(token.value)

        elif (token.type == 'Keyword'):
            outFile += "{0} line {1} cols {2}-{3} is {4}".format(token.value, token.lineno, token.column, (token.column + len(valueAsStr) - 1), token.value)

        elif token.type == 'BoolNumber':
            outFile += "{0} line {1} cols {2}-{3} is {4} (value = {5})".format(token.strValue, token.lineno,
                                                                               token.column,
                                                                               (token.column + len(token.strValue) - 1),
                                                                               token.type, token.strValue)

        elif token.type in strValuesPrintable:
            outFile += "{0} line {1} cols {2}-{3} is {4} (value = {5})".format(token.strValue, token.lineno, token.column,
                                                                           (token.column + len(token.strValue) - 1),
                                                                           token.type, token.value)

        elif token.type in valuesPrintable:
            outFile += "{0} line {1} cols {2}-{3} is {4} (value = {5})".format(token.strValue, token.lineno, token.column, (token.column + len(token.strValue) - 1), token.type, token.value)

        else:
            outFile += "{0} line {1} cols {2}-{3} is {4}".format(token.value, token.lineno, token.column, (token.column + len(valueAsStr) - 1), token.type)

        outFile += "\n"

    isEqual: bool = (outFile == outDataContent)
    differenceOfFiles: str = ''
    if (not isEqual):
        differenceOfFiles = compareTwoString(outFile, outDataContent, "НашВывод", "Тест")
    testResult: TestResult = TestResult(isEqual, os.path.basename(file), outFile, differenceOfFiles)
    return testResult
