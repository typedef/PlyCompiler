import collections
import copy
from enum import Enum
from re import Pattern

from ply.lex import LexToken
import ply.yacc as yacc
import scanner
import codeHelpers
import re

gParserEndRule = "<END>"
'''
Features implemented:
    * 
    public class SomeClass
    {
        public float floatField = 3.14f;
    }
    
TODO:
    * class declaration
    * interface declaration

'''

def stack_pop(arr: []):
    arrLen = len(arr)
    if arrLen > 0:
        return arr[arrLen - 1]
    return None

class BnfItemType(Enum):
    Default = 0,
    Optional = 1,
    Keyword = 2,
    Token = 3

class ParserItem:
    def __init__(self, atype, avalue: LexToken):
        self.Type = atype
        self.Value = avalue

    def __repr__(self):
        res = "(T:{0} V:{1})".format(self.Type, self.Value.value)
        return res

    def __str__(self):
        return self.__repr__()

class ParserNode:
    def __init__(self, current: ParserItem = None):
        self.Current: ParserItem = current
        self.Parent: ParserNode = None
        self.Children: [ParserNode] = []

    def print(self):
        print(self.Current)
        for child in self.Children:
            child.print()
    def __repr__(self):
        res = "P:" + str(self.Current)
        if self.Children is not None:
            for child in self.Children:
                res += " C: " + str(child.Current)
        return res

    def __str__(self):
        return self.__repr__()

# LL(1)
class LParser:
    def __init__(self):
        self.build_tables()
        self.Tokens = {}
        self.Keywords = []
        self.parser_make_keyword_and_tokens()

    def build_tables(self):
        'Строим таблицы: prediction table(pt), translation table(tt), composition table(ct)'

        '''
        Предсказывае следующий токен два варианта использования: 
        1. Зная текущее правило, мы можем предсказыть следующее
        2. Зная все, что лежит в PredictStack мы можем предсказать следующее правило
        
        [0]                   [1]
        <class modifier>  -> class
        <member modifier> -> <type>
        '''
        pt = {}
        '''
        1.token -> <bnf rule>
        2.Транслируем <bnf rule> (terminal/non-terminal) к более верхноуровневому с 
        точки зрения иерархии составленного BNF правилу 
            token | <bnf rule> -> <bnf rule>
        '''
        tt = {}
        '''
        Ищем родителя для набора <bnf rule>
             <bnf rule>, ... <bnf rule> -> <bnf rule>
        '''
        ct = {}

        pt["<class modifier>"] = ["class"]
        pt["class"] = ["<ref type>"]
        pt["<class body>"] = [
            # We use this for understanding that forward-prediction
            # isn't working for element and bp is needed
            gParserEndRule
        ]
        pt["<CurlyBracketsLeft>"] = [
            "<class body declaration list>"
        ]
        pt["<class body declaration list>"] = [
            "<CurlyBracketsRight>"
        ]
        pt["<Identifier>"] = [
            "<oop implements>",
            "<opp extends>",
            "<class body>"
        ]
        pt["<DotComma>"] = [
            gParserEndRule
        ]
        pt["<ctor modifiers>"] = [
            "<Identifier>"
        ]
        pt["<member modifier>"] = [
            "<type>"
        ]
        pt["<field or method modifier>"] = [
            # <field modifier list> <--- overcomplicates everything, mb .simplify() for tt
            "<type>"
        ]
        pt["<field modifier list>"] = ["<type>"]
        pt["<type>"] = ["<Identifier>"]
        pt["<Identifier>"] = ["<DotComma>"]
        # Class prediction
        pt["<class modifier> class <Identifier>"] = [
            "<class body>"
        ]
        pt["<class modifier> class <Identifier> <opp extends>"] = [
            "<class body>"
        ]
        pt["<class modifier> class <Identifier> <oop implements>"] = [
            "<class body>"
        ]
        pt["<class modifier> class <Identifier> <opp extends> <oop implements>"] = [
            "<class body>"
        ]
        pt["<class modifier> class <Identifier> <oop implements> <opp extends>"] = [
            "<class body>"
        ]
        # Interface prediction
        pt["interface <Identifier>"] = [
            "<interface body>"
        ]
        # Field member
        pt["<member modifier> <type> <Identifier>"] = [
            "<DotComma>"
        ]
        # Ctor
        pt["<ctor modifiers> <Identifier> <ParenthesesLeft> <ParenthesesRight>"] = [
            "<ctor body>"
        ]
        pt["<Identifier> <ParenthesesLeft> <ParenthesesRight>"] = [
            "<ctor body>"
        ]
        # Var creation
        pt["<type> <Identifier> <Assign> <literal>"] = [
            "<DotComma>"
        ]
        # Field assign
        pt["this <AccessOperator> <Identifier> <Assign> <literal>"] = [
            "<DotComma>"
        ]

        tt["public"] = ["<ctor modifiers>", "<member modifier>", "<class modifier>"]
        # tt["<Identifier>"] = [
        #     "<ref type>"
        # ]
        convertedToType = [ "<ref type>", "<default type>" ]
        for ctt in convertedToType:
            tt[ctt] = [ "<type>" ]

        tt["<class body declaration list>"] = [
            "<class body>"
        ]
        tt["<value type>"] = [
            "<default type>"
        ]
        valueTypes = [
            "i8", "i16", "i32", "i64", "u8", "u16", "u32", "u64", "f32", "f64",
            "byte", "short", "int", "long", "float", "double",
            "char", "bool"
        ]
        for v in valueTypes:
            tt[v] = ["<value type>"]


        defaultTypes = [ "void", "String", "string" ]
        for dt in defaultTypes:
            tt[dt] = [ "<default type>" ]

        literals = [
            "<FloatNumber>", "<DoubleNumber>", "<IntegerNumber>", "<BoolNumber>", "<StringLiteral>", "<CharLiteral>",
            "null"
        ]
        for l in literals:
            tt[l] = [ "<literal>" ]

        # Class
        ct["<class modifier> class <Identifier> <class body>"] = "<class declaration>"
        ct["<class modifier> class <Identifier> <opp extends> <class body>"] = "<class declaration>"
        ct["<class modifier> class <Identifier> <oop implements> <class body>"] = "<class declaration>"
        ct["<class modifier> class <Identifier> <opp extends> <oop implements> <class body>"] = "<class declaration>"
        ct["<class modifier> class <Identifier> <oop implements> <opp extends> <class body>"] = "<class declaration>"
        # Interface
        ct["interface <Identifier> <interface body>"] = "<interface declaration>"

        # Class member
        ct["<member modifier> <member static modifier> <type> <Identifier> <DotComma>"] = "<field declaration>"
        ct["<member static modifier> <member modifier> <type> <Identifier> <DotComma>"] = "<field declaration>"
        ct["<member modifier> <type> <Identifier> <DotComma>"] = "<field declaration>"
        ct["<member static modifier> <type> <Identifier> <DotComma>"] = "<field declaration>"
        ct["<type> <Identifier> <DotComma>"] = "<field declaration>"

        # Ctor
        ct["<ctor modifiers> <Identifier> <ParenthesesLeft> <ParenthesesRight> <ctor body>"] = "<ctor declaration>"
        ct["<Identifier> <ParenthesesLeft> <ParenthesesRight> <ctor body>"] = "<ctor declaration>"

        # Variable assign
        ct["<type> <Identifier> <Assign> <literal> <DotComma>"] = "<Variable declaration>"

        #Field assign
        ct["this <AccessOperator> <Identifier> <Assign> <literal> <DotComma>"] = "<Field assign>"

        # Interface method decl
        ct["<type> <Identifier> <ParenthesesLeft> <ParenthesesRight> <DotComma>"] = "<interface method>"


        # Проверки на корректность заполнения, это нужно из-за динамической тупизации Python :?
        for key in pt:
            if isinstance(pt[key], str):
                codeHelpers.error("key {0} val {1}".format(key, pt[key]))
                raise Exception("not array registered inside pt!")
        for key in tt:
            if isinstance(tt[key], str):
                codeHelpers.error("key {0} val {1}".format(key, tt[key]))
                raise Exception("not array registered inside tt!")
        for key in ct:
            if not isinstance(ct[key], str):
                codeHelpers.error("key {0} val {1}".format(key, ct[key]))
                raise Exception("not scalar registered inside ct!")

        self.pt = pt
        self.tt = tt
        self.ct = ct

    def parser_make_keyword_and_tokens(self):
        for token in scanner.tokens:
            ptoken = "<{0}>".format(token)
            self.Tokens[ptoken] = ptoken
        self.Keywords = scanner.keyWords

    def __parser_items_to_str(self, predictStack: [ParserItem]) -> str:
        'Конвертирует predictStack в строку из ParserItem.Type, записанных через пробел'
        res: str = ''
        i: int = 0
        psLen: int = len(predictStack)
        last: int = psLen - 1
        while i < psLen:
            ps: ParserItem = predictStack[i]
            res += ps.Type
            if i != last:
                res += ' '
            i += 1
        return res

    def parser_compress(self, predictStack: [ParserItem], result: [ParserNode], token: LexToken) -> ParserNode:
        '''
        Берет массив <bnf rule> из predict stack и пытается найти родителя и создает иерархическую структуру
        '''
        # Compression (for interfaces)
        predictStr = self.__parser_items_to_str(predictStack)
        canBeCompressed: bool = predictStr in self.ct
        if canBeCompressed:
            parent = self.ct[predictStr]
            parentNode = ParserNode(ParserItem(parent, token))
            for child in predictStack:
                parentNode.Children.append(ParserNode(child))
            predictStack.clear()
            result.append(parentNode)
            return parentNode
        return None

    def parser_process_new(self, tokens: [LexToken]) -> [ParserNode]:
        # По умолчанию все коллекции пустые
        result: [ParserNode] = []
        predictStack: [ParserItem] = []
        tokensLen = len(tokens)

        i: int = 0
        predictStr: str = ''
        token: LexToken = None
        while i < tokensLen:
            # Конвертируем predictStack к строке
            predictStr = self.__parser_items_to_str(predictStack)
            # Проверяем эту строку на наличие в таблице pt
            canBePredicted: bool = (len(predictStack) > 1) and (predictStr in self.pt)

            token = tokens[i]

            if canBePredicted:
                # Predict Str может предсказать следующий <bnf item>
                cv = self.pt[predictStr]
                predictStack.append(ParserItem(cv[0], token))

                # Пытаемся ужать выражение
                parentNode: ParserNode = self.parser_compress(predictStack, result, token)
                if parentNode is None:
                    raise Exception("Syntax Error")

                # Idea: Разбираем иерархическую структуру по типу class, interface, ctor, method

                # Вырезаем токены из массива для разрабора (всё что между скобок)
                lastChild: ParserNode = stack_pop(parentNode.Children)
                sliceArr = tokens[i:]
                clousureInd: int = self.skip_to_proper_token_with_nesting(sliceArr, "{", "}")
                sliceArr = sliceArr[1: clousureInd]

                # Рекурсивно падаем в разбор (Нисходящий)
                # Берем все токены из слайса и парсим их как ноды синтаксического дерева (sliceArr -> [ParentNode])
                lastChild.Children = self.parser_process_new(sliceArr)

                # Skip разобранных <bnf item>
                i += clousureInd
            else:
                # Idea: predictStr содержит недостаточно <bnf item> для предсказания следующего <bnf item>
                # convert token -> <bnf item>
                tokenAsStr: str = codeHelpers.tokensToStr([token])

                # Терминалы не из таблицы просто кладутся в predictStack как ParserItem
                pickedTerm = self.parser_pick_terminal_that_not_in_tt(tokenAsStr)
                if pickedTerm is not None:
                    predictStack.append(ParserItem(pickedTerm, token))
                    # Пытаемся поулчить родителя для <bnf item>
                    self.parser_compress(predictStack, result, token)
                    i += 1
                    continue

                # <bnf item> определяем, что это за <bnf rule> в tt
                potentialVals = self.tt[tokenAsStr]
                nextToken = tokens[i + 1]
                nextTokenAsStr: str = codeHelpers.tokensToStr([nextToken])
                pickedNodeAsStr: ParserItem = self.pick_by_forward_prediction(token, potentialVals, nextTokenAsStr)
                predictStack.append(pickedNodeAsStr)

                # Пытаемся ужать выражение
                self.parser_compress(predictStack, result, token)

            i += 1

        return result

    def pick_by_forward_prediction(self, token: LexToken, potentialVals, nextTokenAsStr: str) -> ParserItem:
        if len(potentialVals) == 1:
            pv = potentialVals[0]
            if pv not in self.tt:
                return ParserItem(pv, token)
            while pv in self.tt:
                pvs = self.tt[pv]
                if len(pvs) > 1:
                    raise Exception("Not translatable tt key {0}".format(pv))
                pv = pvs[0]
            return ParserItem(pv, token)

        for v in potentialVals:
            ptVals = self.pt[v]
            for ptv in ptVals:
                if self.isTerminal(ptv):
                    if ptv == nextTokenAsStr:
                        return ParserItem(v, token)
                else:
                    print("Non terminal check {0} -> {1}".format(ptv, nextTokenAsStr))
                    if self.is_non_terminal_suitable(ptv, nextTokenAsStr):
                        return ParserItem(v, token)

        codeHelpers.error("Syntax error expected smth from this list")
        print(potentialVals)
        raise Exception("Syntax error")

    def is_non_terminal_suitable(self, ptv: str, tokensAsStr: str):
        if tokensAsStr not in self.tt: #ptv not in self.tt or
            codeHelpers.warn("Token {0} doesn't exist in tt".format(tokensAsStr))
            return False

        translatedList = self.tt[tokensAsStr]
        while len(translatedList) > 0:
            newList = []
            for tl in translatedList:
                if ptv == tl:
                    return True
                if tl in self.tt:
                    newList.extend(self.tt[tl])

            translatedList = newList
        return False

    def skip_to_proper_token_with_nesting(self, tokens: [LexToken], ptoken: str, mtoken: str):
        'Returns index of proper minusToken in col tokens'
        counterInd = 0
        tokenIndex = 0

        for token in tokens:
            if token.value == ptoken:
                tokenIndex += 1
            elif token.value == mtoken:
                tokenIndex -= 1
            if tokenIndex == 0:
                break
            counterInd += 1

        return counterInd

    def parser_pick_terminal_that_not_in_tt(self, tokenAsStr: str) -> str:
        if tokenAsStr not in self.tt:
            isReallyTerm: bool = (tokenAsStr[0] != "<") \
                                 or (tokenAsStr in self.Tokens) \
                                 or (tokenAsStr in self.Keywords)
            if isReallyTerm:
                return tokenAsStr
        return None

    def isTerminal(self, tokenAsStr):
        if tokenAsStr[0] != "<" or tokenAsStr in self.Tokens:
            return True
        return False

def run() -> [ParserNode]:
    path = "../resources/Small.java" # "../resources/JavaSampleProgramLexer.java"
    javaCode = codeHelpers.readFileAsString(path)
    tokens = scanner.getTokens(javaCode)

    lparser: LParser = LParser()
    parserResult: [ParserNode] = lparser.parser_process_new(tokens)

    for node in parserResult:
        print("Parser result: {0}".format(node.Current))
        for child in node.Children:
            print("Childrens: {0}".format(child))

    return parserResult
