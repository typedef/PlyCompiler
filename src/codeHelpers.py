import _io
import os
import random

from ply.lex import LexToken


def getRandomAsciiString(length):
    asciiChars = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    ]
    asciiCharsUpper = []
    for i in asciiChars:
        asciiCharsUpper.append(i.upper())
    digits = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ]

    allChars = asciiChars + asciiCharsUpper + digits

    result = ''
    for i in range(length):
        result += random.choice(allChars)
    return result

def makeOneSpaceString(str):
    return ' '.join(str.split())

def readFileAsString(filePath: str):
    file: _io.TextIOWrapper = open(filePath, 'r', encoding="utf-8")
    fileContent = file.read()
    file.close()
    return fileContent


def writeFileAsString(filePath: str, data: str):
    file: _io.TextIOWrapper = open(filePath, 'w+', encoding="utf-8")
    fileContent: str = file.write(data)
    file.close()
    return fileContent

def debug_WriteTokensToFile(tokens):
    tcnt = 0
    tokensAsStr = ""

    for ind in range(0, len(tokens)):
        tok = tokens[ind]
        if not hasattr(tok, "type"):
            continue
        print("[main] " + tok.type, tok.value)  # , tok.lineno, tok.lexpos

        val = str(tok.value)
        if ((ind + 1) < (len(tokens) - 1)):
            nextVal = str(tokens[ind + 1].value)

        if (val == ";"):
            tokensAsStr += val + "\n"

            toInd = tcnt
            if (nextVal == "}"):
                toInd = tcnt - 1
            for i in range(0, toInd):
                tokensAsStr += "\t"
        elif (val == "{" or val == "}"):
            if (val == "}"):
                tcnt -= 1
                tokensAsStr += val + "\n\n"
            elif (val == "{"):
                tcnt += 1
                tokensAsStr += val + "\n"

            toInd = tcnt
            if (nextVal == "}"):
                toInd = tcnt - 1
            for i in range(0, toInd):
                tokensAsStr += "\t"
        else:
            tokensAsStr += val + " "

    writeFileAsString("../resources/JavaSampleProgramLexer.java", tokensAsStr)

def removeWrongIdentation(fileContent):
    fileLines = fileContent.split("\n")
    newFileContent = ''
    for line in fileLines:
        newLine = line.rstrip()
        if (len(newLine) > 0):
            newFileContent += makeOneSpaceString(newLine) + '\n'
    return newFileContent

def recreateAllTestButWithEqualIdentation():
    newDir = "../fixed_tests"
    if not os.path.exists(newDir):
        os.makedirs(newDir)

    mappings = {
        "T_Identifier": "Identifier",
        "T_IntConstant": "IntegerNumber",
        "T_DoubleConstant": "DoubleNumber",
        "T_String": "string",
        "T_Int": "int",
        "T_Void": "void",
        "T_Class": "class",
        "T_Double": "double",
        "T_Extends": "extends",
        "T_New": "new",
        "T_While": "while",
        "T_If": "if",
        "T_Else": "else",
        "T_This": "this",
        "T_Return": "return",
        "T_Break": "break",
        "T_BoolConstant": "BoolNumber",
        "T_Or": "Or",
        "T_LessEqual": "LessEqual",
        "T_GreaterEqual": "MoreEqual",
        "T_Equal": "Equal",
        "T_ReadInteger": "Identifier",
        "T_ReadLine": "Identifier",
        "T_Print": "Identifier",
        "T_NewArray": "Identifier",
        "newArray": "Identifier",
        "Or": "OrBinary",
        "OrBinary": "Or",

        "stringConstant": "StringLiteral",

        "'{'": "CurlyBracketsLeft",
        "'}'": "CurlyBracketsRight",
        "'('": "ParenthesesLeft",
        "')'": "ParenthesesRight",
        "';'": "DotComma",
        "'.'": "AccessOperator",
        "'/'": "Divide",
        "'+'": "Plus",
        "'-'": "Minus",
        "'='": "Assign",
        "'!'": "Not",
        "'*'": "Times",
        "'<'": "Less",
        "'>'": "More",
        "','": "Comma",

        "1200": "1200.0",
        "1220": "1220.0",
    }

    oldDir = "../lexer_tests"
    files = os.listdir(oldDir)
    for file in files:
        newFilePath = newDir + '/' + file
        fileContent = readFileAsString(oldDir + '/' + file)
        if file.find(".out") != -1:
            newFileContent = removeWrongIdentation(fileContent)

            for mapping in mappings:
                newFileContent = newFileContent.replace(mapping, mappings[mapping])

            writeFileAsString(newFilePath, newFileContent)
        else:
            writeFileAsString(newFilePath, fileContent)

class PrintColor:
    RED = '\033[91m'
    YELLOW = '\033[93m'
    GREEN = '\033[92m'
    END = '\033[0m'

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def warn(s):
    print(PrintColor.YELLOW + s + PrintColor.END)

def error(s):
    print(PrintColor.RED + s + PrintColor.END)

def success(s):
    print(PrintColor.GREEN + s + PrintColor.END)


def strsToString(str: []):
    return ', '.join(str)

def tokensToStr(tokens: [LexToken]):
    res = ''
    tokensLen = len(tokens) - 1
    for i, token in enumerate(tokens):
        if token.type != 'Keyword':
            res += "<{0}>".format(token.type)
        else:
            res += token.value

        if i < tokensLen:
            res += " "

    return res