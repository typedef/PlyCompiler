public class Main {
	public class BaseClass {
		public bool isBaseClass ( ) {
			return true ;
		}

	}

	public class StaticClass extends BaseClass {
		private String _field ;
		public StaticClass ( ) {
			_field = "Empty" ;
		}

		public String getField ( ) {
			return _field ;
		}

		public void setField ( String field ) {
			this . _field = field ;
		}

		public void printImportantMessage ( ) {
			System . out . println ( "Very important message! " + _field ) ;
		}

		@Override public bool isBaseClass ( ) {
			return false ;
		}

	}

	public static void print ( s ) {
		System . out . println ( s ) ;
	}

	public static void main ( String [] args ) {
		int i0 = 23 ;
		int i1 = 127.43 ;
		int i2 = 10000000000.0 ;
		int i3 = 0.0025 ;
		float i4 = ( 123.0 + 345 * ( 45 - 33 ) + 14 / 2 ) / 2334.0 ;
		int i6 = 65535 ;
		int i7 = 1 ;
		int i8a = 0.12 ;
		int i8b = 12.0 ;
		int i9 = 12 ;
		int fv = 12.0 ;
		int fv2 = 0.12 ;
		int fv3 = 3.14 ;
		bool isTrue = false ;
		bool isFalse = true ;
		String str = "Some string" ;
		string str2233 = "Раз два три 'Четыре пять'" ;
		string str2244 = "Раз два три \"Четыре пять\"" ;
		char c = 'A' ;
		if ( i0 > i1 ) {
			print ( "i0(" + i0 + ") > i1(" + i1 + ")" ) ;
		}

		else if ( i0 == i1 ) {
			print ( "i0 == i1" ) ;
		}

		if ( i2 > = i3 && i4 > 0.0 ) {
		}

		StaticClass sc = new StaticClass ( ) ;
		sc . printImportantMessage ( ) ;
		int ifintthis = 12340 ;
		int [] array0 = new int [5] {
			1 , 2 , 3 , 4 , 5 }

		;
		int g_CNJsTlPhZ30HaykA4RFmWlJmV0IE0 = 123 ;
		if ( g_CNJsTlPhZ30HaykA4RFmWlJmV0IE0 > 100 ) {
			System . out . println ( "Bigger" ) ;
		}

		else {
			System . out . println ( "Not Bigger" ) ;
		}

		if ( int this ) {
		}

		if ( 23 this ) {
		}

	}

}

