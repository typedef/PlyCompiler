/*

This is code sample
For ply lexer
bla bla bla
bla bla bla
bla bla bla
bla bla bla

wrong numbers: 10. E+1

*/

public class Main
{

    public class BaseClass
    {
        public bool isBaseClass()
        {
            return true;
        }
    }

    public interface IInterface
    {
        void doSmth();
    }

     public class StaticClass extends BaseClass
     {
	 private String _field;

	public StaticClass()
	{
	    _field = "Empty";
	}

	public String getField()
	{
	    return _field;
	}
	public void setField(String field)
	{
	    this._field = field;
	}

	public void printImportantMessage()
	{
	  System.out.println("Very important message! " + _field);
	}

	@Override
	public bool isBaseClass()
	{
	    return false;
	}

      }

    public static void print(s)
    {
	System.out.println(s);
    }


    public static void main(String[] args)
	{
	    // ' 12E 127.43 1.0E+10 2.5e-3 145.0e 123 + 345 * ( 45 – 33) / 2334 '
	    int i0 = 23;
	    int i1 = 127.43;
	    int i2 = 1.0E+10;
	    int i3 = 2.5e-3;
	    float i4 = (123.0 + 345 * (45 - 33) + 14 / 2) / 2334.0f;
	    int i6 = 0xffff;
	    int i7 = 0X1;
	    int i8a = .12;
	    int i8b = 12.;
	    int i9 = 012;
	    int fv = 12.f;
	    int fv2 = .12f;
	    int fv3 = 3.14f;
	    bool isTrue = false;
	    bool isFalse = true;
	    String str = "Some string";
	    string str2233 = "Раз два три 'Четыре пять'";
	    string str2244 = "Раз два три \"Четыре пять\"";
	    char c = 'A';

	    if (i0 > i1)
	    {
		print("i0(" + i0 + ") > i1(" + i1 + ")");
	    }
	    else if (i0 == i1)
	    {
		print("i0 == i1");
	    }

	    if (i2 >= i3 && i4 > 0.0f)
	    {
		// do smth
	    }

	    //int iError = 12E;

	    StaticClass sc = new StaticClass();
	    sc.printImportantMessage();

	    int ifintthis = 12340;

        // Array
        int[] array0 = new int[5] {
            1, 2, 3, 4, 5
        };


		// Errors
	    int verybigidentifierfirefirefiresomemorewords = 123;
	    if (verybigidentifierfirefirefiresomemorewords > 100) {
	        System.out.println("Bigger");
	    }
	    else {
	        System.out.println("Not Bigger");
	    }

		if(int this) {}

        if(23this) {}

	}
}
